-- chikun :: 2015
-- A tutorial covering the basics of LÖVE



-- Performed at game startup
function love.load()
    
    -- Set background colour to a purple
    love.graphics.setBackgroundColor(64, 32, 96)
    
    -- Variable which contains the score
    score = 0
    
    -- Create our player table
    player = {
        x = 64,     -- Set player x
        y = 64,     -- Set player y
        w = 32,     -- Set player width
        h = 32,     -- Set player height
        image = love.graphics.newImage("gfx/player.png")
    }
    
    -- Load our coin image
    coinImage =   love.graphics.newImage("gfx/coin.png")
    
    -- Load our coin sound
    coinCollect = love.audio.newSource("sfx/coin.ogg")
    
    -- Create a table of coins
    coins = {
        {
            x = 128,  y = 96,
            w = 16,   h = 16
        },
        {
            x = 256,  y = 386,
            w = 16,   h = 16
        },
        {
            x = 16,   y = 256,
            w = 16,   h = 16
        },
        {
            x = 480,  y = 128,
            w = 16,   h = 16
        },
        {
            x = 640,  y = 386,
            w = 16,   h = 16
        }
    }
    
end



--[[
    Performed on game update. Happens as fast as possible.
    dt means Delta Time, which is the time in seconds since
    last game update. If synced to 60 FPS, dt will be equal
    to 1/60
  ]]
function love.update(dt)
    
    
    -- Player horizontal movement, if keyboard buttons pressed
    if (love.keyboard.isDown('left')) then
        player.x = (player.x - 192 * dt)
    end
    if (love.keyboard.isDown('right')) then
        player.x = (player.x + 192 * dt)
    end
    
    
    -- Player vertical movement, if keyboard buttons pressed
    if (love.keyboard.isDown('up')) then
        player.y = (player.y - 192 * dt)
    end
    if (love.keyboard.isDown('down')) then
        player.y = (player.y + 192 * dt)
    end
    
    
    -- Player collecting coins
    
    -- Iterate through all coins
    for key, coin in ipairs(coins) do
        
        -- Check if coin touches player
        if (isColliding(player, coin)) then
            
            -- Play coin sound
            coinCollect:play()
            
            -- Increase score
            score = score + 1
            
            -- Delete coin from table
            table.remove(coins, key)
            
        end
        
    end
    
    
end



-- Performed on game draw
function love.draw()
    
    -- Set drawing colour to white [default]
    love.graphics.setColor(255, 255, 255)
    
    -- Iterate through coins
    for key, coin in ipairs(coins) do
        
        -- Draw coins at their places
        love.graphics.draw(coinImage, coin.x, coin.y)
        
    end
    
    -- Draw player at its position
    love.graphics.draw(player.image, player.x, player.y)
    
    -- Draw score
    love.graphics.print("Score: " .. score, 32, 32)
    
end



-- Check if two objects are colliding
function isColliding(object1, object2)
    
    -- Determines if the two objects are actually colliding
    if (object1.x < object2.x + object2.w and
        object2.x < object1.x + object1.w and
        object1.y < object2.y + object2.h and
        object2.y < object1.y + object1.h) then
        return(true)
    end
    
    -- Else, return false
    return(false)
    
end