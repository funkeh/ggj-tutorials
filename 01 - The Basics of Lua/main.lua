-- chikun :: 2015
-- A tutorial covering the basics of Lua


-- This is a comment

--[[
  This is a multi-line comment
  ]]



--[[
    We will now go through most of the data types
  ]]



-- If a variable doesn't exist, it is considered nil
if (a == nil) then
    print("a is equal to nil")
end


-- You define a variable (or local variable) like this
g = 5
local h = 9


-- There are no doubles, floats, integers, etc., only numbers
x, y, z = 32, 64.6, -50
print(x + y + z)    -- Will print 46.6



-- There are many ways to make strings
string1, string2, string3 =
    'Single quotes',
    "Double quotes",
    [[
Multi-line strings
    ]]

--[[
    You must add numbers and concatenate strings.
    Conversion is automatically performed.
    Addition is performed before concatenation.
  ]]
print("6" * 6)  -- Will print 36
print("6" .. 6) -- Will print 66
print("6" .. 6 + 6) -- Will print 612



-- Printing new lines is done with \n
print("x\nh")



-- Functions can be defined in many ways
function returnNegative(value)
    return(-value)
end
returnSquare = function(value)
    return(value * value)
end
printClone = print      -- Actually valid
printClone("Success")



-- Boolean values are fairly straight forward
markIsCool, markIsUgly = true, true

-- Let's correct this. You can invert like this
markIsUgly = not markIsUgly     --> Becomes false



--[[
    Arrays and objects are combined in one data type: Tables
  ]]

--[[
    Unpaired tables are like arrays
    This means that the values in the table are not paired
    with variable names.
  ]]
unpairedTable = { 1, 2, 5, 4 }
print(#unpairedTable)      --> Number of unpaired items in unpairedTable [4]
print(unpairedTable[3])    --> Prints third value in unpairedTable [5]

-- Paired tables are like objects
pairedTable = {
    name = "Henry",
    position ={
        x = 32,
        y = 48,
        z = 24
    },
    rename = function(newName)
        pairedTable.name = newName
    end
}
print(#pairedTable)         --> Number of unpaired items in pairedTable [0]
print(pairedTable.name)     --> Print name [Henry]
pairedTable.rename("Josef") --> Rename Henry
print(pairedTable["name"])  --> Also print name [Josef]
print(pairedTable["position"].x)    --> Prints x [32]
print(pairedTable["position"]["y"]) --> Prints y [48]
print(pairedTable.position.z)       --> Prints z [24]

-- You can combine types
mixedTable = {
    5,
    4,
    x = 3
}
print(#mixedTable)      --> 2
print(mixedTable.x)     --> 3
print(mixedTable[1])    --> 5

-- Tables are incredibly powerful and will be used very frequently



--[[
    You can determine a variable type by using the
    type() function
  ]]
print(type(a))              --> nil
print(type(x))              --> number
print(type(string1))        --> string
print(type(printClone))     --> function
print(type(returnNegative)) --> function
print(type(markIsCool))     --> boolean



-- Mathematical conventions
print(6 + 2)    --> 8 [add]
print(6 - 2)    --> 4 [subtract]
print(6 * 2)    --> 12 [multiply]
print(6 / 2)    --> 3 [divide]
print(6 ^ 2)    --> 36 [power]
print(6 % 5)    --> 1 [modulo]

-- Logical operators
print(4 and 5)         --> 5
print(nil and 13)      --> nil
print(false and 13)    --> false
print(4 or 5)          --> 4
print(false or 5)      --> 5



-- If statement

-- If both variables are different then...
if (markIsUgly ~= markIsCool) then
    print("Everything is OK")
elseif (markIsUgly == markIsCool) then
    print("This shouldn't happen")
else
    print("This is impossible")
end



-- For statement

-- Regular for loop
for i = 1, 5, 2 do
    print(i)
end
--[[ Prints:
    1
    3
    5
  ]]

-- You don't need a final number if incrementing by 1
for i = 1, 10 do
    print(i)
end
--[[ Prints:
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
  ]]

-- NOTE: The difference between pairs and ipairs is important

-- Iterate through an unpaired table
newTable = { 4, "three", 2, "1" }
for key, value in ipairs(newTable) do
    print(key .. " // " .. value)
end
--[[ Prints:
    1 // 4
    2 // three
    3 // 2
    4 // 1
  ]]

-- Iterate through a paired table
newTable = {
    x = 32,
    y = 48,
    name = "Henry"
}
for key, value in pairs(newTable) do
    print(key .. " // " .. value)
end
--[[ Prints:
    y // 48
    x // 32
    name // Henry
  ]]



-- While do statement

iterator = 0
while (true) do
    iterator = iterator + 1
    if (iterator > 5) then
        break
    end
end
print("while loop ended")




-- Repeat statement
iterator = 0
repeat
    iterator = iterator + 1
until (iterator < 5)
print("repeat loop ended")



--[[
    End of tutorial 1
  ]]