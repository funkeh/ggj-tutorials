-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local winState = { }


-- On state create
function winState:create()

    
    -- Set background colour
    g.setBackgroundColor(192, 128, 64)
    
    -- Play win noise
    sfx.win:play()
    
    
end


-- On state update
function winState:update(dt)

end


-- On state draw
function winState:draw()
    
    
    -- Set colour
    g.setColor(0, 0, 0)
    
    -- Set font
    g.setFont(fnt.splash)
    
    -- Draw logo with formatting
    g.printf("You Win!", 227, 240 - (fnt.splash:getHeight() / 2), 400, 'center')
    

end


-- On state kill
function winState:kill()

end


-- Transfer data to state loading script
return winState
