-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On play state create
function playState:create()

    
    -- Set background colour to a purple
    g.setBackgroundColor(96, 32, 64)
    
    
    -- Play the background music
    bgm.loop:play()
    
    
    -- Set our map to the test map
    map.setCurrent(maps.test)
    
    -- This is how we interpret map data
    for key, layer in ipairs(map.current.layers) do
        
        if (layer.name == "important") then
            
            for key, object in ipairs(layer.objects) do
                
                if (object.name == "playerStart") then
                
                    -- Set the player's position
                    player.set(object.x, object.y,
                               object.w, object.h)
                    
                end
            end
            
        elseif (layer.name == "coins") then
            
            -- Set the coins table
            coins = layer.objects
            
        elseif (layer.name == "collisions") then
            
            -- Set the collisions table
            collisions = layer.objects
            
        end
    end
    
    
    -- Create our view object
    view = {
        x = 0,
        y = 0,
        w = 854,
        h = 480
    }
    
    
end


-- On play state update
function playState:update(dt)

    
    -- Update player
    player.update(dt)
    
    
    -- Update view to follow player, but restrict it based on map data
    view.x = math.clamp(0, player.x + player.w / 2 - view.w / 2,
        map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, player.y + player.h / 2 - view.h / 2,
        map.current.h * map.current.tileH - view.h)
    
    
end


-- On play state draw
function playState:draw()
    
    
    -- Move drawing position of objects based on view position
    g.translate(-view.x, -view.y)
    
    -- Draw the map
    map.draw()
    
    -- Draw player at its position
    player.draw()
    
    -- Reset the drawing position
    g.origin()
    
    -- Draw GUI here
    
    -- Set drawing font
    g.setFont(fnt.regular)
    
    -- Draw player score
    g.print("Score: " .. player.score, 16, 16)
    

end


-- On play state kill
function playState:kill()
    
    
    -- Kill variables
    coins       = nil
    collisions  = nil
    
    
    -- Stop the background music
    bgm.loop:stop()

    
end


-- Transfer data to state loading script
return playState
