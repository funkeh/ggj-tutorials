# A few simple tutorials for chikun in GGJ
To run these tutorials, open a Command Prompt / Terminal and change the directory to a tutorial's folder.

Run the tutorial using

```
#!lua

love .
```

These are more like examples than tutorials.